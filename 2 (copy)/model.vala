struct Pos {
    public int x;
    public int y;
}

class Model : Object {
    
    public Pos pos; 
    public Pos old_pos; 
    
    public int[,] z;
    public bool[,] y;
    
    StringBuilder output;

    public Model() {
    
        output = new StringBuilder();
    
        pos.x = 1;
        pos.y = 1;

        old_pos.x = 1;
        old_pos.y = 1;
    
        z = {{5,2,3,1},
             {2,4,1,5},
             {4,3,5,4},
             {3,3,2,3},
             {2,1,4,1}};
    
        y = {{true,false,false,false},
             {false,false,false,false},
             {false,false,false,false},
             {false,false,false,false},
             {false,false,false,false}};
    }
    
    public string to_string(){
        var builder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            builder.append (@"x$(i+1): ");
    
            for (int j = 0; j < 5; j++) 
                builder.append (@"$(z[j,i]), ");
            
            builder.append("\n");
        }
        builder.append("\n\n");

        for (int i = 0; i < 4; i++) {
            builder.append (@"x$(i+1): ");

            for (int j = 0; j < 5; j++) 
                builder.append (@"$(y[j,i]), ");
            
            builder.append("\n");
        }
        return builder.str;
    }

    public void input(int g){
       

        int a = z[pos.x-1,g-1]; // выбор пользователя
        stdout.printf (@"Вы выбрали $a\n");
        stdout.printf (@"Переход в ячейку с содержимым $(z[a-1,pos.y-1])\n");


        y[pos.x-1,pos.y-1] = false;
        y[a-1,g-1]     = true; 

        old_pos.x = pos.x;
        old_pos.y = pos.y;

        pos.x = a;
        pos.y = g;
       
    }

    public int output_check(){
        if (pos.x < 3) {output.append("1, "); return 1;}
        else if (3 < pos.x < 5) {output.append("2, "); return 2;}
        else  { output.append("3, "); return 3;}
    }


    public void main_loop(){
        bool exit = true;
        while (exit) {
           
            print(@"$this\n");
            
            stdout.printf ("Возможные переходы в состояния: ");
            for (int i = 0; i < 4; i++) {
                print(@"$(z[pos.x-1,i]), ");
            }
            print("\n\n");
            input(int.parse(stdin.read_line ()));
            output_check();
            stdout.printf ("Выход: " + output.str + "\n");
    
        }
    }
}

//  static int main(string[] args) {
//      var sas = new Model();
//      sas.main_loop();

//      //sas.y[0,0] = 0;
//      return 0;
//  }