/* view.c generated by valac 0.46.2, the Vala compiler
 * generated from view.vala, do not modify */

#include <gtk/gtk.h>
#include <glib-object.h>

#define TYPE_LABEL_GRID (label_grid_get_type ())
#define LABEL_GRID(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_LABEL_GRID, LabelGrid))
#define LABEL_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_LABEL_GRID, LabelGridClass))
#define IS_LABEL_GRID(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_LABEL_GRID))
#define IS_LABEL_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_LABEL_GRID))
#define LABEL_GRID_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_LABEL_GRID, LabelGridClass))

typedef struct _LabelGrid LabelGrid;
typedef struct _LabelGridClass LabelGridClass;
typedef struct _LabelGridPrivate LabelGridPrivate;
enum  {
	LABEL_GRID_0_PROPERTY,
	LABEL_GRID_NUM_PROPERTIES
};
static GParamSpec* label_grid_properties[LABEL_GRID_NUM_PROPERTIES];
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

struct _LabelGrid {
	GtkGrid parent_instance;
	LabelGridPrivate * priv;
	GtkLabel* zx11;
	GtkLabel* zx21;
	GtkLabel* zx31;
	GtkLabel* zx41;
	GtkLabel* zx51;
	GtkLabel* zx12;
	GtkLabel* zx22;
	GtkLabel* zx32;
	GtkLabel* zx42;
	GtkLabel* zx52;
	GtkLabel* zx13;
	GtkLabel* zx23;
	GtkLabel* zx33;
	GtkLabel* zx43;
	GtkLabel* zx53;
	GtkLabel* zx14;
	GtkLabel* zx24;
	GtkLabel* zx34;
	GtkLabel* zx44;
	GtkLabel* zx54;
};

struct _LabelGridClass {
	GtkGridClass parent_class;
};

static gpointer label_grid_parent_class = NULL;

GType label_grid_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (LabelGrid, g_object_unref)
LabelGrid* label_grid_new (void);
LabelGrid* label_grid_construct (GType object_type);
static void label_grid_finalize (GObject * obj);

LabelGrid*
label_grid_construct (GType object_type)
{
	LabelGrid * self = NULL;
#line 49 "../view.vala"
	self = (LabelGrid*) g_object_new (object_type, NULL);
#line 49 "../view.vala"
	return self;
#line 70 "view.c"
}

LabelGrid*
label_grid_new (void)
{
#line 49 "../view.vala"
	return label_grid_construct (TYPE_LABEL_GRID);
#line 78 "view.c"
}

static void
label_grid_class_init (LabelGridClass * klass,
                       gpointer klass_data)
{
#line 3 "../view.vala"
	label_grid_parent_class = g_type_class_peek_parent (klass);
#line 3 "../view.vala"
	G_OBJECT_CLASS (klass)->finalize = label_grid_finalize;
#line 3 "../view.vala"
	gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (klass), "/org/gavr/calc/grid.ui");
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx11", FALSE, G_STRUCT_OFFSET (LabelGrid, zx11));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx21", FALSE, G_STRUCT_OFFSET (LabelGrid, zx21));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx31", FALSE, G_STRUCT_OFFSET (LabelGrid, zx31));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx41", FALSE, G_STRUCT_OFFSET (LabelGrid, zx41));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx51", FALSE, G_STRUCT_OFFSET (LabelGrid, zx51));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx12", FALSE, G_STRUCT_OFFSET (LabelGrid, zx12));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx22", FALSE, G_STRUCT_OFFSET (LabelGrid, zx22));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx32", FALSE, G_STRUCT_OFFSET (LabelGrid, zx32));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx42", FALSE, G_STRUCT_OFFSET (LabelGrid, zx42));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx52", FALSE, G_STRUCT_OFFSET (LabelGrid, zx52));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx13", FALSE, G_STRUCT_OFFSET (LabelGrid, zx13));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx23", FALSE, G_STRUCT_OFFSET (LabelGrid, zx23));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx33", FALSE, G_STRUCT_OFFSET (LabelGrid, zx33));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx43", FALSE, G_STRUCT_OFFSET (LabelGrid, zx43));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx53", FALSE, G_STRUCT_OFFSET (LabelGrid, zx53));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx14", FALSE, G_STRUCT_OFFSET (LabelGrid, zx14));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx24", FALSE, G_STRUCT_OFFSET (LabelGrid, zx24));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx34", FALSE, G_STRUCT_OFFSET (LabelGrid, zx34));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx44", FALSE, G_STRUCT_OFFSET (LabelGrid, zx44));
#line 3 "../view.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "zx54", FALSE, G_STRUCT_OFFSET (LabelGrid, zx54));
#line 131 "view.c"
}

static void
label_grid_instance_init (LabelGrid * self,
                          gpointer klass)
{
#line 3 "../view.vala"
	gtk_widget_init_template (GTK_WIDGET (self));
#line 140 "view.c"
}

static void
label_grid_finalize (GObject * obj)
{
	LabelGrid * self;
#line 3 "../view.vala"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_LABEL_GRID, LabelGrid);
#line 6 "../view.vala"
	_g_object_unref0 (self->zx11);
#line 8 "../view.vala"
	_g_object_unref0 (self->zx21);
#line 10 "../view.vala"
	_g_object_unref0 (self->zx31);
#line 12 "../view.vala"
	_g_object_unref0 (self->zx41);
#line 14 "../view.vala"
	_g_object_unref0 (self->zx51);
#line 17 "../view.vala"
	_g_object_unref0 (self->zx12);
#line 19 "../view.vala"
	_g_object_unref0 (self->zx22);
#line 21 "../view.vala"
	_g_object_unref0 (self->zx32);
#line 23 "../view.vala"
	_g_object_unref0 (self->zx42);
#line 25 "../view.vala"
	_g_object_unref0 (self->zx52);
#line 28 "../view.vala"
	_g_object_unref0 (self->zx13);
#line 30 "../view.vala"
	_g_object_unref0 (self->zx23);
#line 32 "../view.vala"
	_g_object_unref0 (self->zx33);
#line 34 "../view.vala"
	_g_object_unref0 (self->zx43);
#line 36 "../view.vala"
	_g_object_unref0 (self->zx53);
#line 39 "../view.vala"
	_g_object_unref0 (self->zx14);
#line 41 "../view.vala"
	_g_object_unref0 (self->zx24);
#line 43 "../view.vala"
	_g_object_unref0 (self->zx34);
#line 45 "../view.vala"
	_g_object_unref0 (self->zx44);
#line 47 "../view.vala"
	_g_object_unref0 (self->zx54);
#line 3 "../view.vala"
	G_OBJECT_CLASS (label_grid_parent_class)->finalize (obj);
#line 191 "view.c"
}

GType
label_grid_get_type (void)
{
	static volatile gsize label_grid_type_id__volatile = 0;
	if (g_once_init_enter (&label_grid_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (LabelGridClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) label_grid_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (LabelGrid), 0, (GInstanceInitFunc) label_grid_instance_init, NULL };
		GType label_grid_type_id;
		label_grid_type_id = g_type_register_static (gtk_grid_get_type (), "LabelGrid", &g_define_type_info, 0);
		g_once_init_leave (&label_grid_type_id__volatile, label_grid_type_id);
	}
	return label_grid_type_id__volatile;
}

