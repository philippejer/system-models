/* model.c generated by valac 0.46.2, the Vala compiler
 * generated from model.vala, do not modify */

#include <glib-object.h>
#include <glib.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define TYPE_POS (pos_get_type ())
typedef struct _Pos Pos;

#define TYPE_MODEL (model_get_type ())
#define MODEL(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MODEL, Model))
#define MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MODEL, ModelClass))
#define IS_MODEL(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MODEL))
#define IS_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MODEL))
#define MODEL_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MODEL, ModelClass))

typedef struct _Model Model;
typedef struct _ModelClass ModelClass;
typedef struct _ModelPrivate ModelPrivate;
enum  {
	MODEL_0_PROPERTY,
	MODEL_NUM_PROPERTIES
};
static GParamSpec* model_properties[MODEL_NUM_PROPERTIES];
#define _g_string_free0(var) ((var == NULL) ? NULL : (var = (g_string_free (var, TRUE), NULL)))
#define _g_free0(var) (var = (g_free (var), NULL))

struct _Pos {
	gint x;
	gint y;
};

struct _Model {
	GObject parent_instance;
	ModelPrivate * priv;
	Pos pos;
	Pos old_pos;
	gint* z;
	gint z_length1;
	gint z_length2;
	gboolean* y;
	gint y_length1;
	gint y_length2;
};

struct _ModelClass {
	GObjectClass parent_class;
};

struct _ModelPrivate {
	GString* output;
};

static gint Model_private_offset;
static gpointer model_parent_class = NULL;

GType pos_get_type (void) G_GNUC_CONST;
Pos* pos_dup (const Pos* self);
void pos_free (Pos* self);
GType model_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (Model, g_object_unref)
Model* model_new (void);
Model* model_construct (GType object_type);
gchar* model_to_string (Model* self);
void model_input (Model* self,
                  gint g);
gint model_output_check (Model* self);
void model_main_loop (Model* self);
static void model_finalize (GObject * obj);

Pos*
pos_dup (const Pos* self)
{
	Pos* dup;
#line 1 "../model.vala"
	dup = g_new0 (Pos, 1);
#line 1 "../model.vala"
	memcpy (dup, self, sizeof (Pos));
#line 1 "../model.vala"
	return dup;
#line 85 "model.c"
}

void
pos_free (Pos* self)
{
#line 1 "../model.vala"
	g_free (self);
#line 93 "model.c"
}

GType
pos_get_type (void)
{
	static volatile gsize pos_type_id__volatile = 0;
	if (g_once_init_enter (&pos_type_id__volatile)) {
		GType pos_type_id;
		pos_type_id = g_boxed_type_register_static ("Pos", (GBoxedCopyFunc) pos_dup, (GBoxedFreeFunc) pos_free);
		g_once_init_leave (&pos_type_id__volatile, pos_type_id);
	}
	return pos_type_id__volatile;
}

static inline gpointer
model_get_instance_private (Model* self)
{
	return G_STRUCT_MEMBER_P (self, Model_private_offset);
}

Model*
model_construct (GType object_type)
{
	Model * self = NULL;
	GString* _tmp0_;
	gint* _tmp1_;
	gboolean* _tmp2_;
#line 16 "../model.vala"
	self = (Model*) g_object_new (object_type, NULL);
#line 18 "../model.vala"
	_tmp0_ = g_string_new ("");
#line 18 "../model.vala"
	_g_string_free0 (self->priv->output);
#line 18 "../model.vala"
	self->priv->output = _tmp0_;
#line 20 "../model.vala"
	self->pos.x = 1;
#line 21 "../model.vala"
	self->pos.y = 1;
#line 23 "../model.vala"
	self->old_pos.x = 1;
#line 24 "../model.vala"
	self->old_pos.y = 1;
#line 26 "../model.vala"
	_tmp1_ = g_new0 (gint, 5 * 4);
#line 26 "../model.vala"
	_tmp1_[0] = 5;
#line 26 "../model.vala"
	_tmp1_[1] = 2;
#line 26 "../model.vala"
	_tmp1_[2] = 3;
#line 26 "../model.vala"
	_tmp1_[3] = 1;
#line 26 "../model.vala"
	_tmp1_[4] = 2;
#line 26 "../model.vala"
	_tmp1_[5] = 4;
#line 26 "../model.vala"
	_tmp1_[6] = 1;
#line 26 "../model.vala"
	_tmp1_[7] = 5;
#line 26 "../model.vala"
	_tmp1_[8] = 4;
#line 26 "../model.vala"
	_tmp1_[9] = 3;
#line 26 "../model.vala"
	_tmp1_[10] = 5;
#line 26 "../model.vala"
	_tmp1_[11] = 4;
#line 26 "../model.vala"
	_tmp1_[12] = 3;
#line 26 "../model.vala"
	_tmp1_[13] = 3;
#line 26 "../model.vala"
	_tmp1_[14] = 2;
#line 26 "../model.vala"
	_tmp1_[15] = 3;
#line 26 "../model.vala"
	_tmp1_[16] = 2;
#line 26 "../model.vala"
	_tmp1_[17] = 1;
#line 26 "../model.vala"
	_tmp1_[18] = 4;
#line 26 "../model.vala"
	_tmp1_[19] = 1;
#line 26 "../model.vala"
	self->z = (g_free (self->z), NULL);
#line 26 "../model.vala"
	self->z = _tmp1_;
#line 26 "../model.vala"
	self->z_length1 = 5;
#line 26 "../model.vala"
	self->z_length2 = 4;
#line 32 "../model.vala"
	_tmp2_ = g_new0 (gboolean, 5 * 4);
#line 32 "../model.vala"
	_tmp2_[0] = TRUE;
#line 32 "../model.vala"
	_tmp2_[1] = FALSE;
#line 32 "../model.vala"
	_tmp2_[2] = FALSE;
#line 32 "../model.vala"
	_tmp2_[3] = FALSE;
#line 32 "../model.vala"
	_tmp2_[4] = FALSE;
#line 32 "../model.vala"
	_tmp2_[5] = FALSE;
#line 32 "../model.vala"
	_tmp2_[6] = FALSE;
#line 32 "../model.vala"
	_tmp2_[7] = FALSE;
#line 32 "../model.vala"
	_tmp2_[8] = FALSE;
#line 32 "../model.vala"
	_tmp2_[9] = FALSE;
#line 32 "../model.vala"
	_tmp2_[10] = FALSE;
#line 32 "../model.vala"
	_tmp2_[11] = FALSE;
#line 32 "../model.vala"
	_tmp2_[12] = FALSE;
#line 32 "../model.vala"
	_tmp2_[13] = FALSE;
#line 32 "../model.vala"
	_tmp2_[14] = FALSE;
#line 32 "../model.vala"
	_tmp2_[15] = FALSE;
#line 32 "../model.vala"
	_tmp2_[16] = FALSE;
#line 32 "../model.vala"
	_tmp2_[17] = FALSE;
#line 32 "../model.vala"
	_tmp2_[18] = FALSE;
#line 32 "../model.vala"
	_tmp2_[19] = FALSE;
#line 32 "../model.vala"
	self->y = (g_free (self->y), NULL);
#line 32 "../model.vala"
	self->y = _tmp2_;
#line 32 "../model.vala"
	self->y_length1 = 5;
#line 32 "../model.vala"
	self->y_length2 = 4;
#line 16 "../model.vala"
	return self;
#line 239 "model.c"
}

Model*
model_new (void)
{
#line 16 "../model.vala"
	return model_construct (TYPE_MODEL);
#line 247 "model.c"
}

static gchar*
bool_to_string (gboolean self)
{
	gchar* result = NULL;
#line 37 "glib-2.0.vapi"
	if (self) {
#line 256 "model.c"
		gchar* _tmp0_;
#line 38 "glib-2.0.vapi"
		_tmp0_ = g_strdup ("true");
#line 38 "glib-2.0.vapi"
		result = _tmp0_;
#line 38 "glib-2.0.vapi"
		return result;
#line 264 "model.c"
	} else {
		gchar* _tmp1_;
#line 40 "glib-2.0.vapi"
		_tmp1_ = g_strdup ("false");
#line 40 "glib-2.0.vapi"
		result = _tmp1_;
#line 40 "glib-2.0.vapi"
		return result;
#line 273 "model.c"
	}
}

gchar*
model_to_string (Model* self)
{
	GString* builder = NULL;
	GString* _tmp0_;
	GString* _tmp18_;
	GString* _tmp36_;
	const gchar* _tmp37_;
	gchar* _tmp38_;
	gchar* result = NULL;
#line 39 "../model.vala"
	g_return_val_if_fail (self != NULL, NULL);
#line 40 "../model.vala"
	_tmp0_ = g_string_new ("");
#line 40 "../model.vala"
	builder = _tmp0_;
#line 293 "model.c"
	{
		gint i = 0;
#line 41 "../model.vala"
		i = 0;
#line 298 "model.c"
		{
			gboolean _tmp1_ = FALSE;
#line 41 "../model.vala"
			_tmp1_ = TRUE;
#line 41 "../model.vala"
			while (TRUE) {
#line 305 "model.c"
				GString* _tmp3_;
				gchar* _tmp4_;
				gchar* _tmp5_;
				gchar* _tmp6_;
				gchar* _tmp7_;
				GString* _tmp17_;
#line 41 "../model.vala"
				if (!_tmp1_) {
#line 314 "model.c"
					gint _tmp2_;
#line 41 "../model.vala"
					_tmp2_ = i;
#line 41 "../model.vala"
					i = _tmp2_ + 1;
#line 320 "model.c"
				}
#line 41 "../model.vala"
				_tmp1_ = FALSE;
#line 41 "../model.vala"
				if (!(i < 4)) {
#line 41 "../model.vala"
					break;
#line 328 "model.c"
				}
#line 42 "../model.vala"
				_tmp3_ = builder;
#line 42 "../model.vala"
				_tmp4_ = g_strdup_printf ("%i", i + 1);
#line 42 "../model.vala"
				_tmp5_ = _tmp4_;
#line 42 "../model.vala"
				_tmp6_ = g_strconcat ("x", _tmp5_, ": ", NULL);
#line 42 "../model.vala"
				_tmp7_ = _tmp6_;
#line 42 "../model.vala"
				g_string_append (_tmp3_, _tmp7_);
#line 42 "../model.vala"
				_g_free0 (_tmp7_);
#line 42 "../model.vala"
				_g_free0 (_tmp5_);
#line 346 "model.c"
				{
					gint j = 0;
#line 44 "../model.vala"
					j = 0;
#line 351 "model.c"
					{
						gboolean _tmp8_ = FALSE;
#line 44 "../model.vala"
						_tmp8_ = TRUE;
#line 44 "../model.vala"
						while (TRUE) {
#line 358 "model.c"
							GString* _tmp10_;
							gint* _tmp11_;
							gint _tmp11__length1;
							gint _tmp11__length2;
							gint _tmp12_;
							gchar* _tmp13_;
							gchar* _tmp14_;
							gchar* _tmp15_;
							gchar* _tmp16_;
#line 44 "../model.vala"
							if (!_tmp8_) {
#line 370 "model.c"
								gint _tmp9_;
#line 44 "../model.vala"
								_tmp9_ = j;
#line 44 "../model.vala"
								j = _tmp9_ + 1;
#line 376 "model.c"
							}
#line 44 "../model.vala"
							_tmp8_ = FALSE;
#line 44 "../model.vala"
							if (!(j < 5)) {
#line 44 "../model.vala"
								break;
#line 384 "model.c"
							}
#line 45 "../model.vala"
							_tmp10_ = builder;
#line 45 "../model.vala"
							_tmp11_ = self->z;
#line 45 "../model.vala"
							_tmp11__length1 = self->z_length1;
#line 45 "../model.vala"
							_tmp11__length2 = self->z_length2;
#line 45 "../model.vala"
							_tmp12_ = _tmp11_[(j * _tmp11__length2) + i];
#line 45 "../model.vala"
							_tmp13_ = g_strdup_printf ("%i", _tmp12_);
#line 45 "../model.vala"
							_tmp14_ = _tmp13_;
#line 45 "../model.vala"
							_tmp15_ = g_strconcat (_tmp14_, ", ", NULL);
#line 45 "../model.vala"
							_tmp16_ = _tmp15_;
#line 45 "../model.vala"
							g_string_append (_tmp10_, _tmp16_);
#line 45 "../model.vala"
							_g_free0 (_tmp16_);
#line 45 "../model.vala"
							_g_free0 (_tmp14_);
#line 410 "model.c"
						}
					}
				}
#line 47 "../model.vala"
				_tmp17_ = builder;
#line 47 "../model.vala"
				g_string_append (_tmp17_, "\n");
#line 418 "model.c"
			}
		}
	}
#line 49 "../model.vala"
	_tmp18_ = builder;
#line 49 "../model.vala"
	g_string_append (_tmp18_, "\n\n");
#line 426 "model.c"
	{
		gint i = 0;
#line 51 "../model.vala"
		i = 0;
#line 431 "model.c"
		{
			gboolean _tmp19_ = FALSE;
#line 51 "../model.vala"
			_tmp19_ = TRUE;
#line 51 "../model.vala"
			while (TRUE) {
#line 438 "model.c"
				GString* _tmp21_;
				gchar* _tmp22_;
				gchar* _tmp23_;
				gchar* _tmp24_;
				gchar* _tmp25_;
				GString* _tmp35_;
#line 51 "../model.vala"
				if (!_tmp19_) {
#line 447 "model.c"
					gint _tmp20_;
#line 51 "../model.vala"
					_tmp20_ = i;
#line 51 "../model.vala"
					i = _tmp20_ + 1;
#line 453 "model.c"
				}
#line 51 "../model.vala"
				_tmp19_ = FALSE;
#line 51 "../model.vala"
				if (!(i < 4)) {
#line 51 "../model.vala"
					break;
#line 461 "model.c"
				}
#line 52 "../model.vala"
				_tmp21_ = builder;
#line 52 "../model.vala"
				_tmp22_ = g_strdup_printf ("%i", i + 1);
#line 52 "../model.vala"
				_tmp23_ = _tmp22_;
#line 52 "../model.vala"
				_tmp24_ = g_strconcat ("x", _tmp23_, ": ", NULL);
#line 52 "../model.vala"
				_tmp25_ = _tmp24_;
#line 52 "../model.vala"
				g_string_append (_tmp21_, _tmp25_);
#line 52 "../model.vala"
				_g_free0 (_tmp25_);
#line 52 "../model.vala"
				_g_free0 (_tmp23_);
#line 479 "model.c"
				{
					gint j = 0;
#line 54 "../model.vala"
					j = 0;
#line 484 "model.c"
					{
						gboolean _tmp26_ = FALSE;
#line 54 "../model.vala"
						_tmp26_ = TRUE;
#line 54 "../model.vala"
						while (TRUE) {
#line 491 "model.c"
							GString* _tmp28_;
							gboolean* _tmp29_;
							gint _tmp29__length1;
							gint _tmp29__length2;
							gboolean _tmp30_;
							gchar* _tmp31_;
							gchar* _tmp32_;
							gchar* _tmp33_;
							gchar* _tmp34_;
#line 54 "../model.vala"
							if (!_tmp26_) {
#line 503 "model.c"
								gint _tmp27_;
#line 54 "../model.vala"
								_tmp27_ = j;
#line 54 "../model.vala"
								j = _tmp27_ + 1;
#line 509 "model.c"
							}
#line 54 "../model.vala"
							_tmp26_ = FALSE;
#line 54 "../model.vala"
							if (!(j < 5)) {
#line 54 "../model.vala"
								break;
#line 517 "model.c"
							}
#line 55 "../model.vala"
							_tmp28_ = builder;
#line 55 "../model.vala"
							_tmp29_ = self->y;
#line 55 "../model.vala"
							_tmp29__length1 = self->y_length1;
#line 55 "../model.vala"
							_tmp29__length2 = self->y_length2;
#line 55 "../model.vala"
							_tmp30_ = _tmp29_[(j * _tmp29__length2) + i];
#line 55 "../model.vala"
							_tmp31_ = bool_to_string (_tmp30_);
#line 55 "../model.vala"
							_tmp32_ = _tmp31_;
#line 55 "../model.vala"
							_tmp33_ = g_strconcat (_tmp32_, ", ", NULL);
#line 55 "../model.vala"
							_tmp34_ = _tmp33_;
#line 55 "../model.vala"
							g_string_append (_tmp28_, _tmp34_);
#line 55 "../model.vala"
							_g_free0 (_tmp34_);
#line 55 "../model.vala"
							_g_free0 (_tmp32_);
#line 543 "model.c"
						}
					}
				}
#line 57 "../model.vala"
				_tmp35_ = builder;
#line 57 "../model.vala"
				g_string_append (_tmp35_, "\n");
#line 551 "model.c"
			}
		}
	}
#line 59 "../model.vala"
	_tmp36_ = builder;
#line 59 "../model.vala"
	_tmp37_ = _tmp36_->str;
#line 59 "../model.vala"
	_tmp38_ = g_strdup (_tmp37_);
#line 59 "../model.vala"
	result = _tmp38_;
#line 59 "../model.vala"
	_g_string_free0 (builder);
#line 59 "../model.vala"
	return result;
#line 567 "model.c"
}

void
model_input (Model* self,
             gint g)
{
	gint a = 0;
	gint* _tmp0_;
	gint _tmp0__length1;
	gint _tmp0__length2;
	Pos _tmp1_;
	gint _tmp2_;
	FILE* _tmp3_;
	gchar* _tmp4_;
	gchar* _tmp5_;
	gchar* _tmp6_;
	gchar* _tmp7_;
	FILE* _tmp8_;
	gint* _tmp9_;
	gint _tmp9__length1;
	gint _tmp9__length2;
	Pos _tmp10_;
	gint _tmp11_;
	gchar* _tmp12_;
	gchar* _tmp13_;
	gchar* _tmp14_;
	gchar* _tmp15_;
	gboolean* _tmp16_;
	gint _tmp16__length1;
	gint _tmp16__length2;
	Pos _tmp17_;
	Pos _tmp18_;
	gboolean* _tmp19_;
	gint _tmp19__length1;
	gint _tmp19__length2;
	Pos _tmp20_;
	Pos _tmp21_;
#line 62 "../model.vala"
	g_return_if_fail (self != NULL);
#line 65 "../model.vala"
	_tmp0_ = self->z;
#line 65 "../model.vala"
	_tmp0__length1 = self->z_length1;
#line 65 "../model.vala"
	_tmp0__length2 = self->z_length2;
#line 65 "../model.vala"
	_tmp1_ = self->pos;
#line 65 "../model.vala"
	_tmp2_ = _tmp0_[((_tmp1_.x - 1) * _tmp0__length2) + (g - 1)];
#line 65 "../model.vala"
	a = _tmp2_;
#line 66 "../model.vala"
	_tmp3_ = stdout;
#line 66 "../model.vala"
	_tmp4_ = g_strdup_printf ("%i", a);
#line 66 "../model.vala"
	_tmp5_ = _tmp4_;
#line 66 "../model.vala"
	_tmp6_ = g_strconcat ("Вы выбрали ", _tmp5_, "\n", NULL);
#line 66 "../model.vala"
	_tmp7_ = _tmp6_;
#line 66 "../model.vala"
	fprintf (_tmp3_, "%s", _tmp7_);
#line 66 "../model.vala"
	_g_free0 (_tmp7_);
#line 66 "../model.vala"
	_g_free0 (_tmp5_);
#line 67 "../model.vala"
	_tmp8_ = stdout;
#line 67 "../model.vala"
	_tmp9_ = self->z;
#line 67 "../model.vala"
	_tmp9__length1 = self->z_length1;
#line 67 "../model.vala"
	_tmp9__length2 = self->z_length2;
#line 67 "../model.vala"
	_tmp10_ = self->pos;
#line 67 "../model.vala"
	_tmp11_ = _tmp9_[((a - 1) * _tmp9__length2) + (_tmp10_.y - 1)];
#line 67 "../model.vala"
	_tmp12_ = g_strdup_printf ("%i", _tmp11_);
#line 67 "../model.vala"
	_tmp13_ = _tmp12_;
#line 67 "../model.vala"
	_tmp14_ = g_strconcat ("Переход в ячейку с содержимым ", _tmp13_, "\n", NULL);
#line 67 "../model.vala"
	_tmp15_ = _tmp14_;
#line 67 "../model.vala"
	fprintf (_tmp8_, "%s", _tmp15_);
#line 67 "../model.vala"
	_g_free0 (_tmp15_);
#line 67 "../model.vala"
	_g_free0 (_tmp13_);
#line 70 "../model.vala"
	_tmp16_ = self->y;
#line 70 "../model.vala"
	_tmp16__length1 = self->y_length1;
#line 70 "../model.vala"
	_tmp16__length2 = self->y_length2;
#line 70 "../model.vala"
	_tmp17_ = self->pos;
#line 70 "../model.vala"
	_tmp18_ = self->pos;
#line 70 "../model.vala"
	_tmp16_[((_tmp17_.x - 1) * _tmp16__length2) + (_tmp18_.y - 1)] = FALSE;
#line 71 "../model.vala"
	_tmp19_ = self->y;
#line 71 "../model.vala"
	_tmp19__length1 = self->y_length1;
#line 71 "../model.vala"
	_tmp19__length2 = self->y_length2;
#line 71 "../model.vala"
	_tmp19_[((a - 1) * _tmp19__length2) + (g - 1)] = TRUE;
#line 73 "../model.vala"
	_tmp20_ = self->pos;
#line 73 "../model.vala"
	self->old_pos.x = _tmp20_.x;
#line 74 "../model.vala"
	_tmp21_ = self->pos;
#line 74 "../model.vala"
	self->old_pos.y = _tmp21_.y;
#line 76 "../model.vala"
	self->pos.x = a;
#line 77 "../model.vala"
	self->pos.y = g;
#line 693 "model.c"
}

gint
model_output_check (Model* self)
{
	Pos _tmp0_;
	gint result = 0;
#line 81 "../model.vala"
	g_return_val_if_fail (self != NULL, 0);
#line 82 "../model.vala"
	_tmp0_ = self->pos;
#line 82 "../model.vala"
	if (_tmp0_.x < 3) {
#line 707 "model.c"
		GString* _tmp1_;
#line 82 "../model.vala"
		_tmp1_ = self->priv->output;
#line 82 "../model.vala"
		g_string_append (_tmp1_, "1, ");
#line 82 "../model.vala"
		result = 1;
#line 82 "../model.vala"
		return result;
#line 717 "model.c"
	} else {
		Pos _tmp2_;
		gint _tmp3_;
#line 83 "../model.vala"
		_tmp2_ = self->pos;
#line 83 "../model.vala"
		_tmp3_ = _tmp2_.x;
#line 83 "../model.vala"
		if ((3 < _tmp3_) && (_tmp3_ < 5)) {
#line 727 "model.c"
			GString* _tmp4_;
#line 83 "../model.vala"
			_tmp4_ = self->priv->output;
#line 83 "../model.vala"
			g_string_append (_tmp4_, "2, ");
#line 83 "../model.vala"
			result = 2;
#line 83 "../model.vala"
			return result;
#line 737 "model.c"
		} else {
			GString* _tmp5_;
#line 84 "../model.vala"
			_tmp5_ = self->priv->output;
#line 84 "../model.vala"
			g_string_append (_tmp5_, "3, ");
#line 84 "../model.vala"
			result = 3;
#line 84 "../model.vala"
			return result;
#line 748 "model.c"
		}
	}
}

static gchar*
g_file_stream_read_line (FILE* self)
{
	gint c = 0;
	GString* ret = NULL;
	GString* _tmp3_;
	gchar* result = NULL;
#line 3688 "glib-2.0.vapi"
	g_return_val_if_fail (self != NULL, NULL);
#line 3690 "glib-2.0.vapi"
	ret = NULL;
#line 3691 "glib-2.0.vapi"
	while (TRUE) {
#line 766 "model.c"
		GString* _tmp0_;
		GString* _tmp2_;
#line 3691 "glib-2.0.vapi"
		c = fgetc (self);
#line 3691 "glib-2.0.vapi"
		if (!(c != EOF)) {
#line 3691 "glib-2.0.vapi"
			break;
#line 775 "model.c"
		}
#line 3692 "glib-2.0.vapi"
		_tmp0_ = ret;
#line 3692 "glib-2.0.vapi"
		if (_tmp0_ == NULL) {
#line 781 "model.c"
			GString* _tmp1_;
#line 3693 "glib-2.0.vapi"
			_tmp1_ = g_string_new ("");
#line 3693 "glib-2.0.vapi"
			_g_string_free0 (ret);
#line 3693 "glib-2.0.vapi"
			ret = _tmp1_;
#line 789 "model.c"
		}
#line 3695 "glib-2.0.vapi"
		if (c == ((gint) '\n')) {
#line 3696 "glib-2.0.vapi"
			break;
#line 795 "model.c"
		}
#line 3698 "glib-2.0.vapi"
		_tmp2_ = ret;
#line 3698 "glib-2.0.vapi"
		g_string_append_c ((GString*) _tmp2_, (gchar) c);
#line 801 "model.c"
	}
#line 3700 "glib-2.0.vapi"
	_tmp3_ = ret;
#line 3700 "glib-2.0.vapi"
	if (_tmp3_ == NULL) {
#line 3701 "glib-2.0.vapi"
		result = NULL;
#line 3701 "glib-2.0.vapi"
		_g_string_free0 (ret);
#line 3701 "glib-2.0.vapi"
		return result;
#line 813 "model.c"
	} else {
		GString* _tmp4_;
		const gchar* _tmp5_;
		gchar* _tmp6_;
#line 3703 "glib-2.0.vapi"
		_tmp4_ = ret;
#line 3703 "glib-2.0.vapi"
		_tmp5_ = ((GString*) _tmp4_)->str;
#line 3703 "glib-2.0.vapi"
		_tmp6_ = g_strdup (_tmp5_);
#line 3703 "glib-2.0.vapi"
		result = _tmp6_;
#line 3703 "glib-2.0.vapi"
		_g_string_free0 (ret);
#line 3703 "glib-2.0.vapi"
		return result;
#line 830 "model.c"
	}
#line 3688 "glib-2.0.vapi"
	_g_string_free0 (ret);
#line 834 "model.c"
}

void
model_main_loop (Model* self)
{
	gboolean exit = FALSE;
#line 88 "../model.vala"
	g_return_if_fail (self != NULL);
#line 89 "../model.vala"
	exit = TRUE;
#line 90 "../model.vala"
	while (TRUE) {
#line 847 "model.c"
		gchar* _tmp0_;
		gchar* _tmp1_;
		gchar* _tmp2_;
		gchar* _tmp3_;
		FILE* _tmp4_;
		FILE* _tmp14_;
		gchar* _tmp15_;
		gchar* _tmp16_;
		FILE* _tmp17_;
		GString* _tmp18_;
		const gchar* _tmp19_;
		gchar* _tmp20_;
		gchar* _tmp21_;
		gchar* _tmp22_;
		gchar* _tmp23_;
#line 90 "../model.vala"
		if (!exit) {
#line 90 "../model.vala"
			break;
#line 867 "model.c"
		}
#line 92 "../model.vala"
		_tmp0_ = model_to_string (self);
#line 92 "../model.vala"
		_tmp1_ = _tmp0_;
#line 92 "../model.vala"
		_tmp2_ = g_strconcat (_tmp1_, "\n", NULL);
#line 92 "../model.vala"
		_tmp3_ = _tmp2_;
#line 92 "../model.vala"
		g_print ("%s", _tmp3_);
#line 92 "../model.vala"
		_g_free0 (_tmp3_);
#line 92 "../model.vala"
		_g_free0 (_tmp1_);
#line 94 "../model.vala"
		_tmp4_ = stdout;
#line 94 "../model.vala"
		fprintf (_tmp4_, "Возможные переходы в состояния: ");
#line 887 "model.c"
		{
			gint i = 0;
#line 95 "../model.vala"
			i = 0;
#line 892 "model.c"
			{
				gboolean _tmp5_ = FALSE;
#line 95 "../model.vala"
				_tmp5_ = TRUE;
#line 95 "../model.vala"
				while (TRUE) {
#line 899 "model.c"
					gint* _tmp7_;
					gint _tmp7__length1;
					gint _tmp7__length2;
					Pos _tmp8_;
					gint _tmp9_;
					gchar* _tmp10_;
					gchar* _tmp11_;
					gchar* _tmp12_;
					gchar* _tmp13_;
#line 95 "../model.vala"
					if (!_tmp5_) {
#line 911 "model.c"
						gint _tmp6_;
#line 95 "../model.vala"
						_tmp6_ = i;
#line 95 "../model.vala"
						i = _tmp6_ + 1;
#line 917 "model.c"
					}
#line 95 "../model.vala"
					_tmp5_ = FALSE;
#line 95 "../model.vala"
					if (!(i < 4)) {
#line 95 "../model.vala"
						break;
#line 925 "model.c"
					}
#line 96 "../model.vala"
					_tmp7_ = self->z;
#line 96 "../model.vala"
					_tmp7__length1 = self->z_length1;
#line 96 "../model.vala"
					_tmp7__length2 = self->z_length2;
#line 96 "../model.vala"
					_tmp8_ = self->pos;
#line 96 "../model.vala"
					_tmp9_ = _tmp7_[((_tmp8_.x - 1) * _tmp7__length2) + i];
#line 96 "../model.vala"
					_tmp10_ = g_strdup_printf ("%i", _tmp9_);
#line 96 "../model.vala"
					_tmp11_ = _tmp10_;
#line 96 "../model.vala"
					_tmp12_ = g_strconcat (_tmp11_, ", ", NULL);
#line 96 "../model.vala"
					_tmp13_ = _tmp12_;
#line 96 "../model.vala"
					g_print ("%s", _tmp13_);
#line 96 "../model.vala"
					_g_free0 (_tmp13_);
#line 96 "../model.vala"
					_g_free0 (_tmp11_);
#line 951 "model.c"
				}
			}
		}
#line 98 "../model.vala"
		g_print ("\n\n");
#line 99 "../model.vala"
		_tmp14_ = stdin;
#line 99 "../model.vala"
		_tmp15_ = g_file_stream_read_line (_tmp14_);
#line 99 "../model.vala"
		_tmp16_ = _tmp15_;
#line 99 "../model.vala"
		model_input (self, atoi (_tmp16_));
#line 99 "../model.vala"
		_g_free0 (_tmp16_);
#line 100 "../model.vala"
		model_output_check (self);
#line 101 "../model.vala"
		_tmp17_ = stdout;
#line 101 "../model.vala"
		_tmp18_ = self->priv->output;
#line 101 "../model.vala"
		_tmp19_ = _tmp18_->str;
#line 101 "../model.vala"
		_tmp20_ = g_strconcat ("Выход: ", _tmp19_, NULL);
#line 101 "../model.vala"
		_tmp21_ = _tmp20_;
#line 101 "../model.vala"
		_tmp22_ = g_strconcat (_tmp21_, "\n", NULL);
#line 101 "../model.vala"
		_tmp23_ = _tmp22_;
#line 101 "../model.vala"
		fprintf (_tmp17_, "%s", _tmp23_);
#line 101 "../model.vala"
		_g_free0 (_tmp23_);
#line 101 "../model.vala"
		_g_free0 (_tmp21_);
#line 989 "model.c"
	}
}

static void
model_class_init (ModelClass * klass,
                  gpointer klass_data)
{
#line 6 "../model.vala"
	model_parent_class = g_type_class_peek_parent (klass);
#line 6 "../model.vala"
	g_type_class_adjust_private_offset (klass, &Model_private_offset);
#line 6 "../model.vala"
	G_OBJECT_CLASS (klass)->finalize = model_finalize;
#line 1003 "model.c"
}

static void
model_instance_init (Model * self,
                     gpointer klass)
{
#line 6 "../model.vala"
	self->priv = model_get_instance_private (self);
#line 1012 "model.c"
}

static void
model_finalize (GObject * obj)
{
	Model * self;
#line 6 "../model.vala"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_MODEL, Model);
#line 11 "../model.vala"
	self->z = (g_free (self->z), NULL);
#line 12 "../model.vala"
	self->y = (g_free (self->y), NULL);
#line 14 "../model.vala"
	_g_string_free0 (self->priv->output);
#line 6 "../model.vala"
	G_OBJECT_CLASS (model_parent_class)->finalize (obj);
#line 1029 "model.c"
}

GType
model_get_type (void)
{
	static volatile gsize model_type_id__volatile = 0;
	if (g_once_init_enter (&model_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ModelClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) model_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (Model), 0, (GInstanceInitFunc) model_instance_init, NULL };
		GType model_type_id;
		model_type_id = g_type_register_static (G_TYPE_OBJECT, "Model", &g_define_type_info, 0);
		Model_private_offset = g_type_add_instance_private (model_type_id, sizeof (ModelPrivate));
		g_once_init_leave (&model_type_id__volatile, model_type_id);
	}
	return model_type_id__volatile;
}

